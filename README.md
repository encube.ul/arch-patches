# Arch patches

My personal patchsets for various software, mostly GNOME related (apparently I'm too sensitive to imperfect and unpolished UI). Pathes are kept in different branches reflecting what [`asp`](https://github.com/falconindy/asp) is doing.
